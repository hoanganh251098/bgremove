from PIL import Image, ImageChops
import numpy as np
import colorsys
from skimage import color as skcolor
import math
from functools import lru_cache
from concurrent.futures import ThreadPoolExecutor

@lru_cache(maxsize=2097152)
def calc_delta_e(pixel_a, pixel_b):
    lab_a = skcolor.rgb2lab(pixel_a[:3])
    lab_b = skcolor.rgb2lab(pixel_b[:3])
    delta_L = lab_a[0] - lab_b[0]
    delta_A = lab_a[1] - lab_b[1]
    delta_B = lab_a[2] - lab_b[2]
    c1 = math.sqrt(lab_a[1] ** 2 + lab_a[2] ** 2)
    c2 = math.sqrt(lab_b[1] ** 2 + lab_b[2] ** 2)
    delta_C = c1 - c2
    delta_H = delta_A ** 2 + delta_B ** 2 + delta_C ** 2
    delta_H = 0 if delta_H < 0 else math.sqrt(delta_H)
    sc = 1 + 0.045 * c1
    sh = 1 + 0.015 * c1
    delta_Ckcsc = delta_C / sc
    delta_Hkhsh = delta_H / sh
    i = delta_L ** 2 + delta_Ckcsc ** 2 + delta_Hkhsh ** 2
    r = 0 if i < 0 else math.sqrt(i) * (10 ** 7)
    return r

@lru_cache(maxsize=256)
def calc_rgb_distance(pixel_a, pixel_b):
    return math.sqrt(
        (pixel_a[0] - pixel_b[0]) ** 2 + 
        (pixel_a[1] - pixel_b[1]) ** 2 + 
        (pixel_a[2] - pixel_b[2]) ** 2
        )

@lru_cache(maxsize=999999)
def calc_opacity(pixel_a, pixel_b, threshold, power):
    r = calc_delta_e(pixel_a, pixel_b)
    r = r ** power
    threshold = threshold ** power
    opacity = threshold - r
    opacity = 0 if opacity < 0 else opacity / threshold
    return opacity

def bgremove(img_a_path, img_b_path, output_name, broker):
    img_a = Image.open(img_a_path)
    if len(np.array(img_a)[0][0]) == 3:
        img_a.putalpha(256)
    img_b = Image.open(img_b_path)
    if len(np.array(img_b)[0][0]) == 3:
        img_b.putalpha(256)

    img_a_data = np.array(img_a)
    img_b_data = np.array(img_b)

    reference_pixel_a = None
    reference_pixel_b = None

    futures = []
    with ThreadPoolExecutor(max_workers=4) as executor:
        broker.set_message('Calculating background colors')
        for (row_idx, row) in enumerate(img_a_data):
            for (pixel_idx, pixel) in enumerate(row):
                pixel_b = img_b_data[row_idx][pixel_idx]
                diff = calc_delta_e(tuple(pixel.tolist()), tuple(pixel_b.tolist()))
                if diff > 24:
                    reference_pixel_a = np.array(pixel, copy=True)
                    reference_pixel_b = np.array(pixel_b, copy=True)
                    break

        broker.set_message('Removing background')
        if reference_pixel_a.any() and reference_pixel_b.any():
            last_progress = 0
            done = 0
            for (row_idx, row) in enumerate(img_a_data):
                def check_progress():
                    nonlocal done
                    nonlocal last_progress
                    done += 1
                    progress = done / len(img_a_data) * 100
                    if progress - last_progress >= 1:
                        last_progress = int(progress)
                        broker.set_progress(last_progress)

                def job(row_idx, row):
                    nonlocal img_a_data
                    nonlocal img_b_data
                    check_progress()
                    for (pixel_idx, pixel) in enumerate(row):
                        pixel_b = img_b_data[row_idx][pixel_idx]
                        cond_a = calc_delta_e(tuple(pixel.tolist()), tuple(reference_pixel_a.tolist())) <= 24
                        cond_b = calc_delta_e(tuple(pixel_b.tolist()), tuple(reference_pixel_b.tolist())) <= 24
                        if cond_a and cond_b:
                            stack = [
                                [10, 4],
                                [15, 2],
                            ]
                            opacity = 0
                            for layer in stack:
                                o = calc_opacity(tuple(pixel.tolist()), tuple(pixel_b.tolist()), layer[0], layer[1])
                                opacity = 1 - (1 - opacity) * (1 - o)

                            original_opacity_a = pixel[3] if len(pixel) > 3 else 255
                            original_opacity_b = pixel_b[3] if len(pixel_b) > 3 else 255
                            img_a_data[row_idx][pixel_idx] = (*pixel[:3], opacity * original_opacity_a)
                            img_b_data[row_idx][pixel_idx] = (*pixel_b[:3], opacity * original_opacity_b)
                future = executor.submit(job, row_idx, row)
                futures.append(future)
        for future in futures:
            future.result()
        broker.set_message('Saving')
        Image.fromarray(img_a_data).save(f'output/{output_name}_1.png')
        Image.fromarray(img_b_data).save(f'output/{output_name}_2.png')
    return [f'output/{output_name}_1.png', f'output/{output_name}_2.png']

if __name__ == '__main__':
    main()
