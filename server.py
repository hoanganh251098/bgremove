from flask import Flask, send_from_directory, redirect, request
from flask_cors import CORS
from werkzeug.utils import secure_filename
import uuid
import os
from model.task import TaskModel
from worker import start_task

ALLOWED_EXTENSIONS = {'.png', '.jpg', '.jpeg'}

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 64 * 1024 * 1024
CORS(app)

@app.route('/app')
def homepage_portal():
    return redirect('/app/')

@app.route('/app/', defaults={'path': 'index.html'})
@app.route('/app/<path:path>')
def homepage(path):
    return send_from_directory('vue-project-template/dist', path)

@app.route('/app/output/<path:path>')
def serve_output(path):
    return send_from_directory('output', path)

@app.route('/api/tasks', methods=['GET', 'POST'])
def api_tasks():
    if request.method == 'GET':
        return {
            'data': TaskModel.list_all()
        }
    try:
        type_ = request.json.get('type')
        if not type_:
            return {
                'message': 'Type not provided.'
            }, 400
        url = request.json.get('url')
        if url == None:
            return {
                'message': 'URL not provided.'
            }, 400
        task_id = TaskModel.create({
            'type'  : type_,
            'url'   : url
        })
        return {
            'data': TaskModel.read(task_id)
        }
    except Exception as e:
        return {
            'message': str(e)
        }, 500

@app.route('/api/tasks/<task_id>', methods=['GET', 'PUT', 'DELETE'])
def api_tasks_taskid(task_id):
    try:
        task = TaskModel.read(task_id)
    except:
        return {
            'message': 'Task not found.'
        }, 404
    if request.method == 'GET':
        return {
            'data': task
        }
    elif request.method == 'PUT':
        input_images = request.json.get('input_images', [])
        task['input_images'] += input_images
        TaskModel.update(task)
        return {
            'message': 'Task updated.'
        }
    elif request.method == 'DELETE':
        for image_path in task['input_images'] + task['output_images']:
            os.remove(image_path)
        TaskModel.remove(task_id)
        return {
            'message': 'Task deleted.'
        }
    return 'Not implemented.'

@app.route('/api/tasks/<task_id>/action', methods=['POST'])
def api_tasks_taskid_action(task_id):
    try:
        task = TaskModel.read(task_id)
    except:
        return {
            'message': 'Task not found.'
        }, 404
    action = request.json.get('action')
    if action == 'start':
        if task['status'] != 'pending':
            return {
                'message': 'Task status must be "pending" to start'
            }, 400
        start_task(task)
        return 'OK'
    elif action == 'cancel':
        return {
            'message': 'Not implemented.'
        }
    elif action == 'pause':
        return {
            'message': 'Not implemented.'
        }
    return {
        'message': 'Unrecognized action.'
    }, 400


@app.route('/api/tasks/<task_id>/uploads', methods=['POST'])
def api_tasks_taskid_uploads(task_id):
    f = request.files['file']
    filename = uuid.uuid4().hex
    ext = os.path.splitext(f.filename)[1]
    full_path = f'uploads/{filename}{ext}'
    try:
        task = TaskModel.read(task_id)
    except:
        return {
            'message': 'Task not found.'
        }, 404
    if ext in ALLOWED_EXTENSIONS:
        task['input_images'].append(full_path)
        f.save(full_path)
        TaskModel.update(task)
        return {
            'message': 'Uploaded.'
        }
    return {
        'message': 'File type is not supported.'
    }, 400


app.run(host="localhost", port="8000", debug=False)

