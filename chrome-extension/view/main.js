const helper = {
    executeScript(f, arg) {
        return new Promise(resolve => {
            if(arg != undefined) {
                chrome.tabs.query({ active: true }, function (tabs) {
                    var tab = tabs[0];
                    chrome.scripting.executeScript({
                        target: {
                            tabId: tab.id,
                        },
                        function: () => {
                            chrome.runtime.onMessage.addListener(function onMessage(msg) {
                                chrome.runtime.onMessage.removeListener(onMessage);
                                window.__ = msg;
                            });
                        }
                    }, () => {
                        setTimeout(() => {
                            chrome.scripting.executeScript({
                                target: {
                                    tabId: tab.id
                                },
                                function: f
                            }, r => {
                                resolve(r[0].result);
                            });
                        });
                    });
                    chrome.tabs.sendMessage(tab.id, arg);
                });
            }
            else {
                chrome.tabs.query({ active: true }, function (tabs) {
                    var tab = tabs[0];
                    chrome.scripting.executeScript({
                        target: {
                            tabId: tab.id,
                        },
                        function: f
                    }, r => {
                        resolve(r[0].result);
                    });
                });
            }
        });
    },
    toArray(obj) {
        const result = [];
        for(let i = 0; i < obj.length; ++i) {
            result.push(obj[i]);
        }
        return result;
    },
    getTabURL() {
        return new Promise(resolve => {
            chrome.tabs.query({active: true, lastFocusedWindow: true}, tabs => {
                let url = tabs[0].url;
                resolve(url);
            });
        })
    },
    getSavedTask() {
        return new Promise(async resolve => {
            chrome.storage.local.get([await helper.getTabURL()], async result => {
                const task = result[await helper.getTabURL()];
                resolve(task);
            });
        });
    },
    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
};


function setProgress(progress) {
    const i = progress;
    $('#progressbar').css('width', `${i}%`);
    if(i == 100) {
        $('#progressbar').toggleClass('bg-info', false);
        $('#progressbar').toggleClass('bg-success', true);
        $('#progressbar').toggleClass('progress-bar-striped', false);
        $('#progressbar').toggleClass('progress-bar-animated', false);
    }
    else {
        $('#progressbar').toggleClass('bg-info', true);
        $('#progressbar').toggleClass('bg-success', false);
        $('#progressbar').toggleClass('progress-bar-striped', true);
        $('#progressbar').toggleClass('progress-bar-animated', true);
    }
}

function watchTask(task) {
    $('#rm-bg-btn').attr('disabled', true);
    const intervalJob = () => {
        $.ajax({
            url: `http://localhost:8000/api/tasks/${task._id}`,
            method: 'get'
        })
        .then(async res => {
            const task = res.data;
            if(await helper.getSavedTask()) {
                chrome.storage.local.set({ [await helper.getTabURL()]: task });
                const progress = res.data.external.data.progress ?? 0;
                setProgress(progress);
            }
            if(task.status != 'running') {
                clearInterval(intervalID);
                $('#img-0-output').attr('src', `http://localhost:8000/app/${task.output_images[0]}`);
                $('#img-1-output').attr('src', `http://localhost:8000/app/${task.output_images[1]}`);
                $('#rm-bg-btn').attr('disabled', false);
                return;
            }
        })
        .fail(err => {
            console.error(err);
            $('#rm-bg-btn').attr('disabled', false);
        });
    };
    intervalJob();
    const intervalID = setInterval(intervalJob, 2000);
}

async function checkTask() {
    chrome.storage.local.get([await helper.getTabURL()], async result => {
        const task = result[await helper.getTabURL()];
        if(task) {
            watchTask(task);
        }
        else {
            $('#rm-bg-btn').attr('disabled', false);
        }
    });
}

(async function main() {
    const events = new EventEmitter;
    const nrColors = await helper.executeScript(() => {
        return [...document.querySelectorAll('.m-cart-config__color-wrap2')].filter(e => {
            return !e.querySelector('label').getAttribute('class').includes('heather');
        }).length;
    });
    $('#nr-colors').text(''+nrColors);

    if(nrColors < 2) return;

    checkTask();

    const images = [];

    async function setColors(color0, color1) {
        $('#img-0-url').attr('src', '');
        $('#img-1-url').attr('src', '');
        const colorID_0 = color0 ?? helper.getRandomInt(0, nrColors - 1);
        let colorID_1 = color1 ?? helper.getRandomInt(0, nrColors - 1);
        while(colorID_1 == colorID_0) {
            colorID_1 = helper.getRandomInt(0, nrColors - 1);
        }
        images[0] = await helper.executeScript(() => {
            [...document.querySelectorAll('.m-cart-config__color-wrap2 label')][__].click();
            return document.querySelectorAll('img.preview_front, img.preview')[0].getAttribute('src');
        }, colorID_0);
        images[1] = await helper.executeScript(() => {
            [...document.querySelectorAll('.m-cart-config__color-wrap2 label')][__].click();
            return document.querySelectorAll('img.preview_front, img.preview')[0].getAttribute('src');
        }, colorID_1);
        $('#img-0-url').attr('src', images[0]);
        $('#img-1-url').attr('src', images[1]);
    }
    await setColors(0, 1);

    $('#random-colors-btn').click(async () => {
        setColors();
    });

    $('#clear-all-cache-btn').click(async () => {
        chrome.storage.local.clear();
    });
    $('#clear-one-cache-btn').click(async () => {
        chrome.storage.local.set({ [await helper.getTabURL()]: null });
    });
    $('#rm-bg-btn').click(async () => {
        $.ajax({
            url: 'http://localhost:8000/api/tasks',
            method: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                url: await helper.getTabURL(),
                type: 'manual'
            })
        })
        .then(res => {
            const task = res.data;
            $.ajax({
                url: `http://localhost:8000/api/tasks/${task._id}`,
                method: 'put',
                contentType: 'application/json',
                data: JSON.stringify({
                    input_images: [
                        images[0],
                        images[1]
                    ]
                })
            })
            .then(async res => {
                $.ajax({
                    url: `http://localhost:8000/api/tasks/${task._id}/action`,
                    method: 'post',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        action: 'start'
                    })
                })
                .then(async res => {
                    chrome.storage.local.set({ [await helper.getTabURL()]: task });
                    events.emit('task_running', task);
                });
            });
        })
        .fail(err => {
            console.error(err);
        });
        events.on('task_running', task => {
            watchTask(task);
        });
    });
})();

