from selenium import webdriver
from model.task import TaskModel

class ProcessBroker:
    def __init__(self, task):
        self.task = task

    def set_message(self, msg):
        self.task['message'] = msg
        TaskModel.update(self.task)

    def set_progress(self, progress):
        self.task['external']['data']['progress'] = progress
        TaskModel.update(self.task)


class BaseWorker:
    def __init__(self, task):
        self.driver = None
        self.task = task
        self.is_aborted = None
        self.broker = ProcessBroker(task)

    def get_driver(self):
        if self.driver == None:
            self.driver = webdriver.Chrome('chromedriver/chromedriver.exe')
        return self.driver
    
    def close_driver(self):
        if self.driver:
            self.driver.close()
        self.driver = None

    def abort(self):
        self.is_aborted = True

    def checkpoint(self):
        if self.is_aborted:
            raise Exception('Task aborted.')

    def update_task(self):
        TaskModel.update(self.task)

