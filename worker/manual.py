from worker._base import BaseWorker
import requests
import shutil
import uuid
import bgremove

def download_file(url, filename):
    with requests.get(url, stream=True) as r:
        with open(filename, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

class Worker(BaseWorker):
    def run(self):
        task = self.task
        if len(task['input_images']) < 2:
            raise Exception('Not enough input images.')
        local_input_images = []
        for input_image in task['input_images']:
            if 'http' in input_image:
                filename = uuid.uuid4().hex
                filename = f'download/{filename}.png'
                download_file(input_image, filename)
                local_input_images.append(filename)
            else:
                local_input_images.append(input_image)
        task['input_images'] = local_input_images
        self.update_task()
        output_name = uuid.uuid4().hex
        output_names = bgremove.bgremove(local_input_images[0], local_input_images[1], output_name, self.broker)
        task['output_images'] += output_names
        self.update_task()
