import os
import importlib
from model.task import TaskModel
from concurrent.futures import ThreadPoolExecutor

FILE_BLACKLIST = ['_base', '__init__', '__pycache__']

def file_filter(f):
    return f.replace('.py', '') not in FILE_BLACKLIST

file_list = filter(file_filter, os.listdir('./worker'))

modules = {}

for file_name in file_list:
    module_name = file_name.replace('.py', '')
    modules[module_name] = importlib.import_module('worker.' + file_name.replace('.py', ''))

threadpool = ThreadPoolExecutor(max_workers=4)
    
def get_worker(name):
    return modules.get(name)

def start_task(task):
    def job():
        worker = get_worker(task['type'])
        worker = worker.Worker(task)
        task['status'] = 'running'
        TaskModel.update(task)
        try:
            print('jere')
            worker.run()
            task['status'] = 'finished'
            TaskModel.update(task)
        except Exception as e:
            task['status'] = 'error'
            task['message'] = str(e)
            TaskModel.update(task)
        worker.close_driver()
    task['status'] = 'queued'
    TaskModel.update(task)
    threadpool.submit(job)

