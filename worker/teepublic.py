from worker._base import BaseWorker
import re

class Worker(BaseWorker):
    def run(self):
        driver  = self.get_driver()
        task    = self.task
        url = task.get('external', {}).get('url')
        if not url:
            return None
        driver.get(url)
        input()
        def check_robot_captcha():
            body = driver.find_elements_by_css_selector('body')[0]
            return "_Incapsula_Resource" in body.get_attribute('innerHTML')
        if check_robot_captcha():
            task['status'] = 'error'
            task['message'] = 'Robot captcha'
            return self.update_task()
        color_els = driver.find_elements_by_css_selector('.m-cart-config__color-wrap2')
        def color_filter(color_el):
            return 'Heather' not in color_el.get_attribute('innerHTML')
        color_els = filter(color_filter, color_els)
        color_els_info = [{
            'color': re.search(r'#[a-f0-9]{6};', color_el.get_attribute('innerHTML'), re.IGNORECASE)[0],
            'title': color_el.find_elements_by_css_selector('label')[0].get_attribute('title'),
            'id': color_el.find_elements_by_css_selector('input')[0].get_attribute('id')
        } for color_el in color_els]
        print(color_els_info)
