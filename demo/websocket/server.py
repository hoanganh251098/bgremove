import asyncio
import datetime
import random
import websockets

async def spitout(websocket, path):
    while True:
        name = await websocket.recv()
        await websocket.send(f'<strong>{name}</strong>')
        await asyncio.sleep(0.3)

start_server = websockets.serve(spitout, "127.0.0.1", 5678)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
