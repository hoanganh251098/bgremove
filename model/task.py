from tinydb.storages import JSONStorage
from tinydb.middlewares import CachingMiddleware
from tinydb import TinyDB, Query, where
from model.db import db
import threading

class TaskModel:
    table = db.table('task')
    lock = threading.Lock()

    @classmethod
    def list_all(cls, page=1, page_size=24):
        with cls.lock:
            return cls.table.all()

    @classmethod
    def read(cls, id):
        with cls.lock:
            return cls.table.search(Query()._id == int(id))[0]

    @classmethod
    def create(cls, data):
        doc_id  = len(cls.table)
        type_   = data['type']
        url     = data['url']
        status  = 'pending'
        log     = ''
        message = ''
        input_images = []
        output_images = []
        with cls.lock:
            cls.table.insert({
                '_id'       : doc_id,
                'type'      : type_,
                'status'    : status,
                'input_images': input_images,
                'output_images': output_images,
                'external'  : {
                    'url': url,
                    'data': {}
                },
                'log'       : log,
                'message'   : message,
            })
        return doc_id

    @classmethod
    def update(cls, task):
        with cls.lock:
            cls.table.update(task, Query()._id == task['_id'])

    @classmethod
    def remove(cls, id):
        with cls.lock:
            cls.table.remove(where('_id') == int(id))

